# Javier Pérez assignment :)

The goal of this document is to explain the development of the task that was assigned to me

1. Final UI
![Car Image](src/assets/ui.png)

2. if you had more time to work on the project

- In this test, I could not do it, but I normally do a code review before sending my code.
- Run the sonar Q for the code smell
- I would do the Utest with jest.
- I would do the e2e: As we know this is an important frontend part. I would implement cypress
- I would implement Redux pattern
- I would apply the TDD.
- Implement Smart and Dump components: To isolate the components and containers, in this
way we can separate the logic business from the presentational view.
- The structure of the application as modules, in the future we can reuse each module in other
apps.
- Infinity scroll for hte paginator
- In another hand, I would implement a multi-language dictionary.
- Also, I would implement a guard to be sure that users go to the correct route.
- And finally, I would apply a lazy loading pattern to get better performance.


3. Post Script
- It was a nice and challenging test, thanks for the opportunity


