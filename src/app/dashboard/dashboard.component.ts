import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { startWith } from 'rxjs/operators';

import { Gif } from '../models/image.models';
import { SearchService } from '../services/search.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  searchFormControl = new FormControl('', [
    Validators.required
  ]);
  data: Gif[] = [];
  constructor(private searchService: SearchService) { }
  /**
  * init the data and send to another component
  * @return {q} query string
  */

  ngOnInit(): void {
    this.searchFormControl.valueChanges.pipe(startWith('')).subscribe((q:
      string) => {
      this.searchService.search(q).subscribe(q => this.data = q.data);
    })

  }

}
