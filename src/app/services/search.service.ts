import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { retry } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SearchService {
  URL = `${environment.URL}`
  constructor(private http: HttpClient) { }
  /**
   * http request to get the data from the server
   * @param {q} String : query string
   * @public
   */
  search(q: string): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      params: new HttpParams({
        fromObject: {
          q
        }
      }),
    };
    return this.http
      .get<any>(this.URL, httpOptions)
      .pipe(
        retry(2),
      );
  }

}


