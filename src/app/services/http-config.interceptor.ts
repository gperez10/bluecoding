import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable()

export class HttpConfigInterceptor implements HttpInterceptor {
  constructor() { }
  /**
 * interceptor to add the types and add the appid that server request
 * @param {string} appid : apiKey
 * @public
 */
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!request.headers.has('Content-Type')) {
      request = request.clone({ headers: request.headers.set('Content-Type', 'application/json') });
    }
    request = request.clone({ headers: request.headers.set('Accept', 'application/json') });
    request = request.clone({ params: request.params.set('api_key', environment.KEY) });
    request = request.clone({ params: request.params.set('limit', environment.LIMIT) });

    return next.handle(request).pipe(
      map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
        }
        return event;
      }),
      catchError((error: HttpErrorResponse) => {
        let data = {};
        data = error && error.error && error.error.reason ? error.error.reason : 'No available'
        return throwError(error);
      }));
  }

}
