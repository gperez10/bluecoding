import { Component, Input } from '@angular/core';

import { Gif } from '../models/image.models';

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.css']
})
export class SearchResultComponent {
  @Input() data: Gif[] = [];
  constructor() { }

}
